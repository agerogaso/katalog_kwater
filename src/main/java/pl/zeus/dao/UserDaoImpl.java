package pl.zeus.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.zeus.model.RoleEntity;
import pl.zeus.model.UserEntity;

@Repository
public class UserDaoImpl implements UserDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public void addUser(UserEntity user) {
		this.sessionFactory.getCurrentSession().save(user);
	}

	@SuppressWarnings("unchecked")
	public UserEntity getUser(Integer userId) {
		
		List<UserEntity> user = new ArrayList<UserEntity>();
		Query query = this.sessionFactory.getCurrentSession().createQuery("from UserEntity WHERE user_id = :userId");
		query.setParameter("userId", userId);
		user = (List<UserEntity>) query.list();
		if(user.isEmpty()) {
			return null;
		}
		else {
			return user.get(0);
		}
	}

	@SuppressWarnings("unchecked")
	public UserEntity getUser(String login, String pass) {
		List<UserEntity> user = new ArrayList<UserEntity>();
		Query query = this.sessionFactory.getCurrentSession().createQuery("from UserEntity WHERE userlogin = :login AND userpass = :pass");
		query.setParameter("login", login);
		query.setParameter("pass", pass);
		
		user = (List<UserEntity>) query.list();
		
		if(user.isEmpty()) {
			return null;	
		}
		else {
			return user.get(0);
		}
	}
	
	public UserEntity getUser(String login) {
		List<UserEntity> user = new ArrayList<UserEntity>();
		Query query = this.sessionFactory.getCurrentSession().createQuery("from UserEntity WHERE userlogin = :login");
		query.setParameter("login", login);
		
		user = query.list();
		
		if(user.isEmpty()) {
			return null;
		}
		else {
			return user.get(0);
		}
		
	}

	@SuppressWarnings("unchecked")
	public List<UserEntity> listUsers() {
		return this.sessionFactory.getCurrentSession().createQuery("from UserEntity").list();
	}

	public void deleteUser(Integer userId) {
		UserEntity user = (UserEntity) sessionFactory.getCurrentSession().load(UserEntity.class, userId);
		
		if(user != null) {
			this.sessionFactory.getCurrentSession().delete(user);
		}
	}

	public void updatePass(Integer userId, String newPass) {
		Query query = this.sessionFactory.getCurrentSession().createQuery("UPDATE UserEntity SET userpass = :newpass WHERE user_id = :user_id");
        query.setParameter("user_id", userId);
		query.setParameter("newpass", newPass); 
		query.executeUpdate();
	}


	public void editUser(Integer userId, UserEntity newUser) {
		Query query = this.sessionFactory.getCurrentSession().createQuery("UPDATE UserEntity SET imie = :newimie, nazwisko = :newnazwisko, telefon = :newtelefon, email = :newemail WHERE user_id = :user_id");
        
        query.setParameter("user_id", userId);
        
		query.setParameter("newimie", newUser.getImie());
        query.setParameter("newnazwisko", newUser.getNazwisko());
        query.setParameter("newtelefon", newUser.getTelefon());
        query.setParameter("newemail", newUser.getEmail());
        
		query.executeUpdate();
	}


	

}
