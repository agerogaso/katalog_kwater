package pl.zeus.dao;

import pl.zeus.model.RoleEntity;

public interface RoleDAO {
	public RoleEntity getRole(int id);
}
