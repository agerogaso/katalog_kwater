package pl.zeus.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.zeus.model.CityEntity;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CityDAOImpl implements CityDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<CityEntity> getCities() {
        return this.sessionFactory.getCurrentSession().createQuery("from CityEntity").list();
    }

    @Override
    public List<CityEntity> getCity(String cityName) {
        List<CityEntity> city = new ArrayList<CityEntity>();
        Query query = this.sessionFactory.getCurrentSession().createQuery("from CityEntity WHERE name = :cityName");
        query.setParameter("cityName",cityName);

        city = (List<CityEntity>) query.list();

        return city;
     /*
        if(city.isEmpty()) {
            return null;
        }
        else {
            return city.get(0);
        }*/
    }
}
