package pl.zeus.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.zeus.model.RoleEntity;

@Repository
public class RoleDaoImpl implements RoleDAO {

	@Autowired
    private SessionFactory sessionFactory;
     
    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
 
    public RoleEntity getRole(int id) {
        RoleEntity role = (RoleEntity) getCurrentSession().load(RoleEntity.class, id);
        return role;
    }

}
