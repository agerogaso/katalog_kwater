package pl.zeus.dao;

import java.util.List;

import pl.zeus.model.ApartmentEntity;

public interface ApartmentDAO {

	public void addApartment(ApartmentEntity apartment);
	public List<ApartmentEntity> getAllApartments();
	public void deleteApartment(Integer apartmentID);
	public ApartmentEntity getApartment(Integer apartmentId);
	public void editApartment(Integer apartmentId, ApartmentEntity newApartment);
}
