package pl.zeus.dao;

import java.util.List;

import javax.sql.DataSource;

import pl.zeus.model.UserEntity;

public interface UserDAO {

	public void addUser(UserEntity user);

	public	UserEntity getUser(Integer userId);

	public UserEntity getUser(String login, String pass);
	
	public UserEntity getUser(String login);
	
	public List<UserEntity> listUsers();

	public void deleteUser(Integer userId);

	public void updatePass(Integer userId, String newPass);
	
	public void editUser(Integer userId, UserEntity newUser);
}
