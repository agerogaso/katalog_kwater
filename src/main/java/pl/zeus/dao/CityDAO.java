package pl.zeus.dao;


import pl.zeus.model.CityEntity;

import java.util.List;

public interface CityDAO {

    public List<CityEntity> getCities();
    public List<CityEntity>  getCity(String cityName);

}
