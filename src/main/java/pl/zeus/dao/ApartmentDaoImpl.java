package pl.zeus.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.zeus.model.ApartmentEntity;

@Repository
public class ApartmentDaoImpl implements ApartmentDAO{

	Date date;
	SimpleDateFormat simplydate;
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void addApartment(ApartmentEntity apartment) {		
		date = new Date();
		simplydate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		apartment.setDodano(simplydate.format(date));
		
		this.sessionFactory.getCurrentSession().save(apartment);
	}

	@SuppressWarnings("unchecked")
	public List<ApartmentEntity> getAllApartments() {
		return this.sessionFactory.getCurrentSession().createQuery("from ApartmentEntity").list();
	}

	public void deleteApartment(Integer apartmentID) {
		ApartmentEntity apartment = (ApartmentEntity) sessionFactory.getCurrentSession().load(ApartmentEntity.class, apartmentID);
		
		if (null != apartment) {
			this.sessionFactory.getCurrentSession().delete(apartment);
		}
		
	}

	public ApartmentEntity getApartment(Integer apartmentId) {
		List<ApartmentEntity> apartment = new ArrayList<ApartmentEntity>();
		Query query = this.sessionFactory.getCurrentSession().createQuery("from ApartmentEntity WHERE id = :apartmentId");
		query.setParameter("apartmentId", apartmentId);
		apartment = (List<ApartmentEntity>) query.list();
		if(apartment.isEmpty()) {
			return null;
		}
		else {
			return apartment.get(0);
		}
	}


	public void editApartment(Integer apartmentId, ApartmentEntity newApartment) {
		System.out.println("apartmentid w daoimpl = "+apartmentId);
		
		String sql = "UPDATE ApartmentEntity SET tytul=:newtytul,ulica=:newulica,dom=:newdom,miasto=:newmiasto,odleglosc=:newodleglosc,stan=:newstan,powierzchnia=:newpowierzchnia,"
				+ "rodzaj=:newrodzaj,pietro=:newpietro,ilosc_pokoi=:newilosc_pokoi,ilosc_miejsc=:newilosc_miejsc,cena_miejsce=:newcena_miejsce,cena_calosc=:newcena_calosc,"
				+ "media_w_cenie=:newmedia_w_cenie,wspolne_wejscie=:newwspolne_wejscie,plec=:newplec,para=:newpara,palacy=:newpalacy,internet=:newinternet,opis_internet=:newopis_internet,"
				+ "kuchnia=:newkuchnia,opis_kuchni=:newopis_kuchni,lazienka=:newlazienka,opis_lazienki=:newopis_lazienki,goscie=:newgoscie,dodatkowe_info=:newdodatkowe_info,"
				+ "zaktualizowano=:newzaktualizowano WHERE id=:apartmentId";
		
		
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
        
        query.setParameter("apartmentId", apartmentId);
        
		query.setParameter("newtytul", newApartment.getTytul());
		query.setParameter("newulica", newApartment.getUlica());
		query.setParameter("newdom", newApartment.getDom());
		query.setParameter("newmiasto", newApartment.getMiasto());
		query.setParameter("newodleglosc", newApartment.getOdleglosc());
		query.setParameter("newstan", newApartment.getStan());
		query.setParameter("newpowierzchnia", newApartment.getPowierzchnia());
		query.setParameter("newrodzaj", newApartment.getRodzaj());
		query.setParameter("newpietro", newApartment.getPietro());
		query.setParameter("newilosc_pokoi", newApartment.getIlosc_pokoi());
		query.setParameter("newilosc_miejsc", newApartment.getIlosc_miejsc());
		query.setParameter("newcena_miejsce", newApartment.getCena_miejsce());
		query.setParameter("newcena_calosc", newApartment.getCena_calosc());
		query.setParameter("newmedia_w_cenie", newApartment.getMedia_w_cenie());
		query.setParameter("newwspolne_wejscie", newApartment.getWspolne_wejscie());
		query.setParameter("newplec", newApartment.getPlec());
		query.setParameter("newpara", newApartment.getPara());
		query.setParameter("newpalacy", newApartment.getPalacy());
		query.setParameter("newinternet", newApartment.getInternet());
		query.setParameter("newopis_internet", newApartment.getOpis_internet());
		query.setParameter("newkuchnia", newApartment.getKuchnia());
		query.setParameter("newopis_kuchni", newApartment.getOpis_kuchni());
		query.setParameter("newlazienka", newApartment.getLazienka());
		query.setParameter("newopis_lazienki", newApartment.getOpis_lazienki());
		query.setParameter("newgoscie", newApartment.getGoscie());
		query.setParameter("newdodatkowe_info", newApartment.getDodatkowe_info());
		
		date = new Date();
		simplydate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//newApartment.setZaktualizowano(simplydate.format(date));
		query.setParameter("newzaktualizowano", simplydate.format(date));
	
        
		query.executeUpdate();
		
	}
	
	
	

}
