package pl.zeus.service;

import pl.zeus.model.RoleEntity;

public interface RoleService {
	public RoleEntity getRole(int id);
}
