package pl.zeus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.zeus.dao.RoleDAO;
import pl.zeus.model.RoleEntity;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDAO roleDAO;

	public RoleEntity getRole(int id) {
		return roleDAO.getRole(id);
	}

}
