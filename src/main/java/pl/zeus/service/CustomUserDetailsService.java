package pl.zeus.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.zeus.dao.UserDAO;
import pl.zeus.model.UserEntity;

@Service
@Transactional(readOnly=true)
public class CustomUserDetailsService implements UserDetailsService{

	@Autowired
	private UserDAO userDAO;
	
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		
		UserEntity userEntity = userDAO.getUser(username);
		
		if(userEntity == null) {
			throw new AuthenticationServiceException("Nie ma takiego uzytkownika w bazie!");
		}
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(userEntity.getRole().getRole()));

        UserDetails user = new User(userEntity.getUserlogin(), userEntity.getUserpass(), true, true, true, true, authorities);

        
        System.out.println(user.getUsername());
        System.out.println(userEntity.getRole().getRole());
		return user;
		
	}


}
