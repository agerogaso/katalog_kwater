package pl.zeus.service;

import pl.zeus.model.CityEntity;

import java.util.List;

public interface CityService {

    public List<CityEntity> getCities();
    public List<CityEntity>  getCity(String cityName);
}
