package pl.zeus.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.zeus.dao.ApartmentDAO;
import pl.zeus.model.ApartmentEntity;

@Service
public class ApartmentServiceImpl implements ApartmentService{

	@Autowired
	private ApartmentDAO apartmentDAO;
	
	public void setApartmentDAO(ApartmentDAO apartmentDAO) {
		this.apartmentDAO = apartmentDAO;
	}
	
	
	@Transactional
	public void addApartment(ApartmentEntity apartment) {
		apartmentDAO.addApartment(apartment);
	}

	@Transactional
	public List<ApartmentEntity> getAllApartments() {
		return apartmentDAO.getAllApartments();
	}

	@Transactional
	public void deleteApartment(Integer apartmentID) {
		apartmentDAO.deleteApartment(apartmentID);	
	}

	@Transactional
	public ApartmentEntity getApartment(Integer apartmentId) {
		return apartmentDAO.getApartment(apartmentId);
	}

	@Transactional
	public void editApartment(Integer apartmentId, ApartmentEntity newApartment) {
		this.apartmentDAO.editApartment(apartmentId, newApartment);
		
	}

}
