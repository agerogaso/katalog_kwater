package pl.zeus.service;

import java.util.List;

import pl.zeus.model.UserEntity;

public interface UserService {

	public void addUser(UserEntity user);

	public	UserEntity getUser(Integer userId);

	public UserEntity getUser(String login, String pass);
	
	public UserEntity getUser(String login);
	
	public List<UserEntity> listUsers();

	public void deleteUser(Integer userId);

	public void updatePass(Integer userId, String newPass);
	
	public void editUser(Integer userId, UserEntity newUser);
}
