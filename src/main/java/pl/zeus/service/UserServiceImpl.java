package pl.zeus.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.zeus.dao.UserDAO;
import pl.zeus.model.UserEntity;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDAO userDAO;
	
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	
	@Transactional
	public void addUser(UserEntity user) {
		this.userDAO.addUser(user);
	}

	@Transactional
	public UserEntity getUser(Integer userId) {
		return this.userDAO.getUser(userId);
	}

	@Transactional
	public UserEntity getUser(String login, String pass) {
		return this.userDAO.getUser(login, pass);
	}

	@Transactional
	public List<UserEntity> listUsers() {
		return this.userDAO.listUsers();
	}

	@Transactional
	public void deleteUser(Integer userId) {
		this.userDAO.deleteUser(userId);
	}

	@Transactional
	public void updatePass(Integer userId, String newPass) {
		this.userDAO.updatePass(userId, newPass);
	}

	@Transactional
	public UserEntity getUser(String login) {
		return this.userDAO.getUser(login);
	}

	@Transactional
	public void editUser(Integer userId, UserEntity newUser) {
		this.userDAO.editUser(userId, newUser);
		
	}

}
