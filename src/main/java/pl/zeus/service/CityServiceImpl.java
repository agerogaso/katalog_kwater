package pl.zeus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zeus.dao.CityDAO;
import pl.zeus.model.CityEntity;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CityServiceImpl implements CityService {

    @Autowired
    private CityDAO cityDAO;

    @Override
    public List<CityEntity> getCities() {
        return cityDAO.getCities();
    }

    @Override
    public List<CityEntity>  getCity(String cityName) {
        return cityDAO.getCity(cityName);
    }
}
