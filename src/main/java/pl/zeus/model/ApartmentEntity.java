package pl.zeus.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "apartments")
public class ApartmentEntity {
	
	@Id
	@Column(name = "id")
	@GeneratedValue
	private int id;

	@Column(name = "tytul")
	public String tytul;

	@Column(name = "ulica")
	public String ulica;

	@Column(name = "dom")
	public String dom;

	@Column(name = "miasto")
	public String miasto;

	@Column(name = "odleglosc")
	public double odleglosc;

	@Column(name = "stan")
	public String stan;

	@Column(name = "powierzchnia")
	public double powierzchnia;

	@Column(name = "rodzaj")
	public String rodzaj;

	@Column(name = "pietro")
	public int pietro;

	@Column(name = "ilosc_pokoi")
	public int ilosc_pokoi;

	@Column(name = "ilosc_miejsc")
	public int ilosc_miejsc;

	@Column(name = "cena_miejsce")
	public double cena_miejsce;

	@Column(name = "cena_calosc")
	public double cena_calosc;

	@Column(name = "media_w_cenie")
	public String media_w_cenie;

	@Column(name = "wspolne_wejscie")
	public String wspolne_wejscie;

	@Column(name = "plec")
	public String plec;

	@Column(name = "para")
	public String para;

	@Column(name = "palacy")
	public String palacy;

	@Column(name = "internet")
	public String internet;

	@Column(name = "opis_internet")
	public String opis_internet;

	@Column(name = "kuchnia")
	public String kuchnia;

	@Column(name = "opis_kuchni")
	public String opis_kuchni;

	@Column(name = "lazienka")
	public String lazienka;

	@Column(name = "opis_lazienki")
	public String opis_lazienki;

	@Column(name = "goscie")
	public String goscie;

	@Column(name = "dodatkowe_info")
	public String dodatkowe_info;

	@Column(name = "dodano")
	public String dodano;

	@Column(name = "zaktualizowano")
	public String zaktualizowano;

	@ManyToOne
	@JoinColumn(name="user_id")
	private UserEntity user;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getDom() {
		return dom;
	}

	public void setDom(String dom) {
		this.dom = dom;
	}

	public String getMiasto() {
		return miasto;
	}

	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}

	public double getOdleglosc() {
		return odleglosc;
	}

	public void setOdleglosc(double odleglosc) {
		this.odleglosc = odleglosc;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public double getPowierzchnia() {
		return powierzchnia;
	}

	public void setPowierzchnia(double powierzchnia) {
		this.powierzchnia = powierzchnia;
	}

	public String getRodzaj() {
		return rodzaj;
	}

	public void setRodzaj(String rodzaj) {
		this.rodzaj = rodzaj;
	}

	public int getPietro() {
		return pietro;
	}

	public void setPietro(int pietro) {
		this.pietro = pietro;
	}

	public int getIlosc_pokoi() {
		return ilosc_pokoi;
	}

	public void setIlosc_pokoi(int ilosc_pokoi) {
		this.ilosc_pokoi = ilosc_pokoi;
	}

	public int getIlosc_miejsc() {
		return ilosc_miejsc;
	}

	public void setIlosc_miejsc(int ilosc_miejsc) {
		this.ilosc_miejsc = ilosc_miejsc;
	}

	public double getCena_miejsce() {
		return cena_miejsce;
	}

	public void setCena_miejsce(double cena_miejsce) {
		this.cena_miejsce = cena_miejsce;
	}

	public double getCena_calosc() {
		return cena_calosc;
	}

	public void setCena_calosc(double cena_calosc) {
		this.cena_calosc = cena_calosc;
	}

	public String getMedia_w_cenie() {
		return media_w_cenie;
	}

	public void setMedia_w_cenie(String media_w_cenie) {
		this.media_w_cenie = media_w_cenie;
	}

	public String getWspolne_wejscie() {
		return wspolne_wejscie;
	}

	public void setWspolne_wejscie(String wspolne_wejscie) {
		this.wspolne_wejscie = wspolne_wejscie;
	}

	public String getPlec() {
		return plec;
	}

	public void setPlec(String plec) {
		this.plec = plec;
	}

	public String getPara() {
		return para;
	}

	public void setPara(String para) {
		this.para = para;
	}

	public String getPalacy() {
		return palacy;
	}

	public void setPalacy(String palacy) {
		this.palacy = palacy;
	}

	public String getInternet() {
		return internet;
	}

	public void setInternet(String internet) {
		this.internet = internet;
	}

	public String getOpis_internet() {
		return opis_internet;
	}

	public void setOpis_internet(String opis_internet) {
		this.opis_internet = opis_internet;
	}

	public String getKuchnia() {
		return kuchnia;
	}

	public void setKuchnia(String kuchnia) {
		this.kuchnia = kuchnia;
	}

	public String getOpis_kuchni() {
		return opis_kuchni;
	}

	public void setOpis_kuchni(String opis_kuchni) {
		this.opis_kuchni = opis_kuchni;
	}

	public String getLazienka() {
		return lazienka;
	}

	public void setLazienka(String lazienka) {
		this.lazienka = lazienka;
	}

	public String getOpis_lazienki() {
		return opis_lazienki;
	}

	public void setOpis_lazienki(String opis_lazienki) {
		this.opis_lazienki = opis_lazienki;
	}

	public String getGoscie() {
		return goscie;
	}

	public void setGoscie(String goscie) {
		this.goscie = goscie;
	}

	public String getDodatkowe_info() {
		return dodatkowe_info;
	}

	public void setDodatkowe_info(String dodatkowe_info) {
		this.dodatkowe_info = dodatkowe_info;
	}


/*
	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}*/

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getDodano() {
		return dodano;
	}

	public void setDodano(String dodano) {
		this.dodano = dodano;
	}

	public String getZaktualizowano() {
		return zaktualizowano;
	}

	public void setZaktualizowano(String zaktualizowano) {
		this.zaktualizowano = zaktualizowano;
	}

	// inne
}
