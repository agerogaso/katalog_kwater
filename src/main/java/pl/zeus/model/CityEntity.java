package pl.zeus.model;


import javax.persistence.*;

@Entity
@Table(name="cities")
public class CityEntity {

    @Id
    @GeneratedValue
    private int id;

    @Column(name="name")
    public String name;

    @Column(name = "voivodeship")
    public String voivodeship;

    public String getVoivodeship() {
        return voivodeship;
    }

    public void setVoivodeship(String voivodeship) {
        this.voivodeship = voivodeship;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
