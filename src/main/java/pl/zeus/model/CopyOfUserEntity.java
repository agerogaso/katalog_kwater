package pl.zeus.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.context.annotation.Scope;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

@Entity
@Table(name = "users")
@Scope("session")
public class CopyOfUserEntity {

	@Id
	@Column(name = "user_id")
	@GeneratedValue
	public int user_id;

	@NotEmpty
	@Column(name = "userlogin")
	public String userlogin;

	@NotEmpty
	@Column(name = "userpass")
	public String userpass;
	
	@NotEmpty
	@Column(name = "imie")
	public String imie;

	@NotEmpty
	@Column(name = "nazwisko")
	public String nazwisko;

	@Email
	@NotEmpty
	@Column(name = "email")
	public String email;

	@NumberFormat(style = Style.NUMBER)
	@NotEmpty
	@Size
	@Column(name = "telefon")
	public String telefon;

	@NotEmpty
	@Transient
	public String userpass2;
	
	@ManyToOne
	@JoinColumn(name="role_id")
	private RoleEntity role;
	
	@OneToMany(mappedBy="user", cascade = { CascadeType.MERGE }, fetch=FetchType.EAGER)
	private List<ApartmentEntity> apartments;
	
	public CopyOfUserEntity() {
	}
	
	
	public CopyOfUserEntity(String login, String imie, String nazwisko, String email,
			String telefon, String pass) {
		super();
		this.userlogin = login;
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.email = email;
		this.telefon = telefon;
		this.userpass = pass;
	}


	public int getUser_id() {
		return user_id;
	}


	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}


	public String getUserlogin() {
		return userlogin;
	}


	public void setUserlogin(String userlogin) {
		this.userlogin = userlogin;
	}


	public String getUserpass() {
		return userpass;
	}


	public void setUserpass(String userpass) {
		this.userpass = userpass;
	}


	public String getImie() {
		return imie;
	}


	public void setImie(String imie) {
		this.imie = imie;
	}


	public String getNazwisko() {
		return nazwisko;
	}


	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTelefon() {
		return telefon;
	}


	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}


	public String getUserpass2() {
		return userpass2;
	}


	public void setUserpass2(String userpass2) {
		this.userpass2 = userpass2;
	}


	public RoleEntity getRole() {
		return role;
	}


	public void setRole(RoleEntity role) {
		this.role = role;
	}


	public List<ApartmentEntity> getApartments() {
		return apartments;
	}


	public void setApartments(List<ApartmentEntity> apartments) {
		this.apartments = apartments;
	}



}
