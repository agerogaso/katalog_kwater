package pl.zeus.form;

import javax.persistence.Column;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class UserUpdateForm {

	@NotEmpty
	@Column(name = "imie")
	public String imie;

	@NotEmpty
	@Column(name = "nazwisko")
	public String nazwisko;

	@Email
	@NotEmpty
	@Column(name = "email")
	public String email;

	@NumberFormat(style = Style.NUMBER)
	@NotEmpty
	@Size(min=9, max=9)
	@Column(name = "telefon")
	public String telefon;

	public UserUpdateForm(String imie, String nazwisko, String email,
			String telefon) {
		super();
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.email = email;
		this.telefon = telefon;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	
	
}
