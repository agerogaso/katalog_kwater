package pl.zeus.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.zeus.model.UserEntity;

public class UserPasswordEditValidator implements Validator {

	String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	String STRING_PATTERN = "[a-zA-Z]+";
	String TEL_PATTERN = "[0-9]{9}";

	private Pattern pattern;
	private Matcher matcher;
	
	public boolean supports(Class clazz) {
		return UserEntity.class.equals(clazz);
	}

	public void validate(Object obj, Errors e) {
		UserEntity user = (UserEntity) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "oldpass", "user.empty.oldpass", "USERPASSWORDEDIT: Has�o nie mo�e by� puste.");
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "newuserpass", "user.empty.newuserpass", "USERPASSWORDEDIT: Nowe has�o nie mo�e by� puste.");
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "newuserpass2", "user.empty.newuserpass2", "USERPASSWORDEDIT: Potwierdzenie has�a nie mo�e by� puste.");
		
		if(!user.getNewuserpass().equals(user.getNewuserpass2())) {
			e.rejectValue("newuserpass", "user.notequals.newuserpass","USERPASSWORDEDIT: Has�a nie s� takie same.");
		}
		
		/*if(!user.getUserpass().equals(user.getOldpass())) {
			e.rejectValue("oldpass", "user.notequals.oldpass","USERPASSWORDEDIT: Has�o nie jest takie same jak stare has�o.");
		}*/
		
	}

}
