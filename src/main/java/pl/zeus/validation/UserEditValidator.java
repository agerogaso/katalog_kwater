package pl.zeus.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.zeus.model.UserEntity;

public class UserEditValidator implements Validator {

	String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	String STRING_PATTERN = "[a-zA-Z]+";
	String TEL_PATTERN = "[0-9]{9}";

	private Pattern pattern;
	private Matcher matcher;
	
	public boolean supports(Class clazz) {
		return UserEntity.class.equals(clazz);
	}

	public void validate(Object obj, Errors e) {
		UserEntity user = (UserEntity) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "imie", "user.empty.imie", "USEREDIT: Imi� nie mo�e by� puste.");
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "nazwisko", "user.empty.nazwisko", "USEREDIT: Nazwisko nie mo�e by� puste.");
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "email", "user.empty.email", "USEREDIT: Email nie mo�e by� pusty.");
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "telefon", "user.empty.telefon", "USEREDIT: Telefon nie mo�e by� pusty.");
		
		if(user.getTelefon() != null) {
			pattern = Pattern.compile(TEL_PATTERN);
			matcher = pattern.matcher(user.getTelefon());
			
			if(!matcher.matches()) {
				e.rejectValue("telefon", "user.notnumeric.telefon","USEREDIT: Wprowad� telefon w dobrym formacie (9 cyfrowy numer)");
			}
		}
		
	}

}
