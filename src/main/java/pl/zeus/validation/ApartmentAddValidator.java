package pl.zeus.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.zeus.model.ApartmentEntity;

public class ApartmentAddValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return ApartmentEntity.class.equals(clazz);
	}

	public void validate(Object obj, Errors e) {
		ApartmentEntity apartment = (ApartmentEntity) obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "tytul", "apartment.empty.tytul", "default error = blank tytul");
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "ulica",  "apartment.empty.ulica", "default error = blank ulica");
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "dom",  "apartment.empty.dom", "default error = blank dom");
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "miasto",  "apartment.empty.miasto", "default error = blank miasto");

	}

}
