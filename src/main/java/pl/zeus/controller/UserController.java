package pl.zeus.controller;

import java.security.Principal;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.zeus.model.ApartmentEntity;
import pl.zeus.model.RoleEntity;
import pl.zeus.model.UserEntity;
import pl.zeus.service.UserService;
import pl.zeus.validation.UserEditValidator;
import pl.zeus.validation.UserPasswordEditValidator;
import pl.zeus.validation.UserRegisterValidator;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	@ModelAttribute("useredit")
	public UserEntity getUser() {
		return new UserEntity();
	}
	
	@ModelAttribute("userregister")
	public UserEntity getUserRegister() {
		return new UserEntity();
	}
	
	@ModelAttribute("userpasswordedit")
	public UserEntity getUserPasswordEdit() {
		return new UserEntity();
	}
	
	@InitBinder("useredit")
	protected void initUserEditBinder(WebDataBinder binder) {
		binder.setValidator(new UserEditValidator());
	}
	
	@InitBinder("userregister")
	protected void initUserRegisterBinder(WebDataBinder binder) {
		binder.setValidator(new UserRegisterValidator());
	}
	
	@InitBinder("userpasswordedit")
	protected void initUserPasswordBinder(WebDataBinder binder) {
		binder.setValidator(new UserPasswordEditValidator());
	}
	
	@RequestMapping(value="/register", method=RequestMethod.GET)
	public String showRegisterForm() {
		return "register";
	}

	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String doRegister(@Valid @ModelAttribute("userregister") UserEntity user,
			BindingResult result, Map model) {

		if(result.hasErrors()) {
			return "register";
		}


		if(!user.getUserlogin().isEmpty() && userService.getUser(user.getUserlogin()) != null) {
			model.put("errLogin", "Ten login jest zajety!");
			return "register";
		}
		
		RoleEntity role = new RoleEntity();
		role.setRole_id(2);
		role.setRole("ROLE_USER");
		
		user.setRole(role);
		userService.addUser(user);	
		model.put("msgtitle", "Zarejestrowa�e� si� pomy�lnie!");
		model.put("msg", "Zarejestrowa�e� si� pomy�lnie! Teraz mo�esz si� zalogowa� u�ywaj�c swojego loginu oraz has�a.");
		return "info";

	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLogin() {
		return "loginform";
	}
	
	@RequestMapping("/logout")
	public String logout() {
		return "logout";
	}
	
	@RequestMapping("/accessdenied")
	public String accessDenied(ModelMap model) {
		model.addAttribute("error", "true");
		return "accessdenied";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(	{"user/profile", "user"}	)
	public String showUserProfile(Map model, Principal principal) {
		model.put("user", userService.getUser(principal.getName()));
		return "user/profile";
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("user/myapartments")
	public String showMyApartments(Map model, Principal principal, HttpServletRequest request) {
		
		UserEntity user = userService.getUser(principal.getName());
		if(user == null) {
			model.put("msgtitle", "Zaloguj si�");
			model.put("msg", "Musisz by� zalogowany aby przegl�da� swoje oferty!");
			return "info";
		}
	
		if(user.getApartments().isEmpty())	{
			model.put("msgtitle", "Moje oferty");
			model.put("msg", "Nie masz �adnych ofert.");
			return "info";
		}
		
		PagedListHolder<ApartmentEntity> pagedListHolder = new PagedListHolder<ApartmentEntity>(user.getApartments());
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);
		pagedListHolder.setPage(page);
		pagedListHolder.setPageSize(6);
		
		model.put("pagedListHolder", pagedListHolder);
		model.put("apartments", user.getApartments());
		
		return "user/myapartments";
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="user/edit", method=RequestMethod.GET)
	public String showUserEdit(Map model, Principal principal) {
		model.put("user", userService.getUser(principal.getName()));
		return "user/edit";
	}
	
	@RequestMapping(value="user/edit", method=RequestMethod.POST)
	public String doUserEdit(@Valid @ModelAttribute("useredit") UserEntity user, BindingResult result, Principal principal) {
		if(result.hasErrors()) {
			return "user/edit";
		}
		this.userService.editUser(userService.getUser(principal.getName()).getUser_id(), user);
		
		return "redirect:/user";
	}
	
	@RequestMapping(value="user/editpassword", method=RequestMethod.GET)
	public String showUserEditPassword() {
		return "user/editpassword";
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="user/editpassword", method=RequestMethod.POST)
	public String doUserEditPassword(@Valid @ModelAttribute("userpasswordedit") UserEntity user, BindingResult result, Principal principal, Map model) {
		if(result.hasErrors()) {
			return "user/editpassword";
		}
		
		if(!user.getOldpass().equals(userService.getUser(principal.getName()).getUserpass())) {
			model.put("errPass", "Stare has�o nie jest prawid�owe.");
			return "user/editpassword";
		}
		
		userService.updatePass(userService.getUser(principal.getName()).getUser_id(), user.getNewuserpass());
		
		model.put("msgtitle", "Zmiana has�a.");
		model.put("msg", "Has�o zosta�o pomy�lnie zmienione. Mo�esz teraz u�ywa� nowego has�a do logowania.");
		return "info";
	}
	
}
