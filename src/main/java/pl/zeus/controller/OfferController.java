package pl.zeus.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.zeus.model.ApartmentEntity;
import pl.zeus.service.ApartmentService;

@Controller
public class OfferController {

    @Autowired
    private ApartmentService apartmentService;

    @ModelAttribute(value = "apartment")
    public ApartmentEntity createApartment() {
        return new ApartmentEntity();
    }


    @RequestMapping(value = "/oferty", method = RequestMethod.GET)
    public String showMap() {

        return "offer/map";
    }


}
