package pl.zeus.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import pl.zeus.model.ApartmentEntity;
import pl.zeus.model.CityEntity;
import pl.zeus.model.UserEntity;
import pl.zeus.service.ApartmentService;
import pl.zeus.service.CityService;
import pl.zeus.service.UserService;
import pl.zeus.validation.ApartmentAddValidator;

@Controller
public class ApartmentController {

	@Autowired
	private ApartmentService apartmentService;

    @Autowired
    private CityService cityService;

	@Autowired
	private UserService userService;

	@Autowired
	ServletContext servletContext;

	@ModelAttribute(value = "apartment")
	public ApartmentEntity createApartment() {
		return new ApartmentEntity();
	}
	
	@InitBinder("apartment")
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(new ApartmentAddValidator());
	}

	@RequestMapping(value = "/apartments", method = RequestMethod.GET)
	public String listApartments(ModelMap model,HttpServletRequest request) {

		if(apartmentService.getAllApartments().isEmpty())	{
			model.put("msgtitle", "Katalog kwater");
			model.put("msg", "Lista ofert jest pusta.");
			return "info";
		}
		
		PagedListHolder<ApartmentEntity> pagedListHolder = new PagedListHolder<ApartmentEntity>(apartmentService.getAllApartments());
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);

		pagedListHolder.setPage(page);
		pagedListHolder.setPageSize(6);
		
		model.put("pagedListHolder", pagedListHolder);
		model.put("apartments", apartmentService.getAllApartments());
		
		return "apartments/apartments";
	}


    @RequestMapping("/map/{_woj}")
    public String ajaxTest(@PathVariable("_woj") String _woj, Map model) {

        return "offer/map";
    }

    @RequestMapping("/jsontest")
    public String jsonTest(HttpServletResponse res, Map model) {
        ObjectMapper mapper = new ObjectMapper();

        String json = null;
        try {
            json = mapper.writeValueAsString(cityService.getCities());
            System.out.print(json);
        } catch (IOException e) {
            e.printStackTrace();
        }

        model.put("json",json);

        return "test";
    }

    @RequestMapping("/jsontest2")
    public @ResponseBody String jsonTest2(HttpServletResponse res) {
        ObjectMapper mapper = new ObjectMapper();

        String json = null;
        try {
            json = mapper.writeValueAsString(cityService.getCities());
            System.out.print(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    @RequestMapping("/jsontest3")
    public @ResponseBody List jsonTest3(@RequestParam("term") String query) {
        List<CityEntity> matched = new ArrayList<CityEntity>();
        List<CityEntity> cities = cityService.getCities();

        System.out.print(query);

        for(CityEntity city : cities) {
            if(city.getName().toLowerCase().startsWith(query.toLowerCase())) {
                matched.add(city);
            }
        }


        return matched;
    }





	@RequestMapping(value = "apartments/add", method = RequestMethod.GET)
	public String showAddApartment() {
		return "apartments/addapartment";
	}

	@RequestMapping(value = "/apartments/add", method = RequestMethod.POST)
	public String addApartment(
			@Valid @ModelAttribute(value = "apartment") ApartmentEntity apartment,
			BindingResult result, Principal principal,
			@RequestParam("file") MultipartFile[] files) {

		if (result.hasErrors()) {
			return "apartments/addapartment";
		}

		apartment.setUser(userService.getUser(principal.getName()));
		apartmentService.addApartment(apartment);

		long filesSize = 0;
		for (MultipartFile file : files) {
			filesSize += file.getSize();
		}
		
		FTPClient ftpClient = new FTPClient();
		
		if (files != null && filesSize>0) {
			
			
			try {
				ftpClient.connect("bd.vot.pl");
				boolean login = ftpClient.login("admin@bd.vot.pl", "harcik321");

				if (login) {
					ftpClient.enterLocalPassiveMode();
					ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

					for (int i = 0; i < files.length; i++) {
						if(!files[i].isEmpty() && files[i].getSize() > 0) {
							byte[] bytes = files[i].getBytes();

							String mainPath = "/public_html/bank_kwater/apartments/";

							boolean dirExists = ftpClient.changeWorkingDirectory(mainPath+apartment.getId());
							if (!dirExists) {
								ftpClient.makeDirectory(mainPath+apartment.getId());
							}
							ftpClient.changeWorkingDirectory("/");
		
							OutputStream outputStream = ftpClient.storeFileStream(mainPath+apartment.getId()+"/zdjecie" + (i+1) + ".jpg");
							outputStream.write(bytes);
							outputStream.close();

							ftpClient.completePendingCommand();
						}
					}

				} else {
					System.out.println("Connection fail...");
				}

			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					ftpClient.disconnect();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		else {
			try {
				ftpClient.connect("bd.vot.pl");
				boolean login = ftpClient.login("admin@bd.vot.pl", "harcik321");
				if (login) {
					ftpClient.enterLocalPassiveMode();
					ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
					
					String mainPath = "/public_html/bank_kwater/apartments/";

					boolean dirExists = ftpClient.changeWorkingDirectory(mainPath+apartment.getId());
					if (!dirExists) {
						ftpClient.makeDirectory(mainPath+apartment.getId());
					}
					ftpClient.changeWorkingDirectory("/");
					
					File defaultImage = new File(getClass().getResource("/defaultphoto.jpg").toURI());

					OutputStream outputStream = ftpClient.storeFileStream(mainPath+apartment.getId()+"/zdjecie1.jpg");
					outputStream.write(IOUtils.toByteArray(new FileInputStream(defaultImage)));
					outputStream.close();
				}
			} catch (SocketException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			} 	
		}

		return "redirect:/apartments/";
	}

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "apartments/{_id}/delete", method = RequestMethod.GET)
	public String showDeleteApartment(@PathVariable("_id") String _id, Map model, Principal principal) {
		if (ApartmentController.isInteger(_id)) {
			ApartmentEntity apartment = apartmentService.getApartment(Integer.parseInt(_id));
			
			UserEntity user = userService.getUser(principal.getName());
			
			if(user == null) {
				model.put("msgtitle", "Zaloguj si�!");
				model.put("msg", "Musisz by� zalogowany �eby to zrobi�!");
				return "info";
			}

			if (apartment.getUser().getUserlogin().equals(user.getUserlogin())) {
				model.put("apartment",apartment);
				model.put("msgtitle", "Usuwanie og�oszenia o id: "+apartment.getId());
				model.put("msg", "Czy na pewno chcesz usun�� to og�oszenie?");
				return "apartments/confirmdelete";
			} else {
				model.put("error", "true");
				model.put("msgtitle", "B��d.");
				model.put("msg", "Nie mo�esz usun�� tej oferty.");
				return "info";
			}

		}
		return "404";
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "apartments/{_id}/delete", method=RequestMethod.POST)
	public String doDeleteApartment(@PathVariable("_id") String _id, Map model) {
		this.apartmentService.deleteApartment(Integer.parseInt(_id));
		return "redirect:/user/myapartments";
	}

	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "apartments/{_id}")
	public String showApartmentById(@PathVariable("_id") String _id, Map model) {
		if (ApartmentController.isInteger(_id)) {
			ApartmentEntity apartment = apartmentService.getApartment(Integer.parseInt(_id));

			if (apartment == null) {
				return "404";
			}

			model.put("user",userService.getUser(apartment.getUser().getUser_id()));
			model.put("apartment", apartment);
			return "apartments/apartment";
		}

		return "404";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "apartments/{_id}/edit", method=RequestMethod.GET)
	public String showApartmentEdit(@PathVariable("_id") String _id, Map model, Principal principal) {

		if (ApartmentController.isInteger(_id)) {
			ApartmentEntity apartment = apartmentService.getApartment(Integer.parseInt(_id));
			if (apartment == null) {
				return "404";
			}
			UserEntity user = userService.getUser(principal.getName());
			
			if(user == null) {
				model.put("msgtitle", "Zaloguj si�!");
				model.put("msg", "Musisz by� zalogowany �eby to zrobi�!");
				return "info";
			}

			if (apartment.getUser().getUserlogin().equals(user.getUserlogin())) {
				
				model.put("user", user);
				model.put("apartment", apartment);
				return "apartments/edit";
			} else {
				model.put("error", "true");
				model.put("msgtitle", "B��d.");
				model.put("msg", "Nie mo�esz edytowa� tej oferty.");
			}
			return "info";
		}

		return "404";
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "apartments/editapartment", method=RequestMethod.POST)
	public String doApartmentEdit(@Valid @ModelAttribute("apartment") ApartmentEntity apartment,  BindingResult result,Map model,
			Principal principal ) {

		if(result.hasErrors()) {
			return "apartments/edit";
		}

			this.apartmentService.editApartment(apartment.getId(), apartment);
		
			return "redirect:/apartments/";
	}


	
	
	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

}
