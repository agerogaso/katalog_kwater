package pl.zeus.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.zeus.model.ApartmentEntity;
import pl.zeus.model.UserEntity;
import pl.zeus.service.ApartmentService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    private ApartmentService apartmentService;

	@ModelAttribute("user")
	public UserEntity getUser() {
		return new UserEntity();
	}

	@RequestMapping("/")
	public String home(@ModelAttribute(value = "user") UserEntity user, ModelMap model, HttpServletRequest request) {
        //loadApartments(model,request);
		return "index";
	}

    @RequestMapping("/test")
    public String test() {
        lol();
        return "test";
    }

    public void lol() {
        String txt = "Bełchatów-Będzin-Biała Podlaska-Białystok-Bielsko-Biała-Bochnia-Brzeg-Brzesko-Bydgoszcz-Bytom-Chełm-Chojnice-Chorzów-Chrzanów-Ciechanów-Częstochowa-Dąbrowa Górnicza-Dębica-Dęblin-Elbląg-Ełk-Gdańsk-Gdynia-Giżycko-Gliwice-Głogów-Gniezno-Gorzów Wielkopolski-Grudziądz-Iława-Inowrocław-Jarocin-Jarosław-Jasło-Jastrzębie-Zdrój-Jaworzno-Jelenia Góra-Józefów-Kalisz-Katowice-Kędzierzyn-Koźle-Kielce-Kołobrzeg-Konin-Kostrzyn nad Odrą-Koszalin-Kraków-Krosno-Kutno-Kwidzyn-Legionowo-Legnica-Leszno-Lidzbark Warmiński-Lubin-Lublin-Łomża-Łowicz-Łódź-Łuków-Mielec-Mińsk Mazowiecki-Mysłowice-Nowa Ruda-Nowy Sącz-Nowy Targ-Nysa-Olecko-Olsztyn-Opole-Ostrołęka-Ostrowiec Świętokrzyski-Ostrów Wielkopolski-Oświęcim-Otwock-Pabianice-Piekary Śląskie-Piła-Pińczów-Piotrków Trybunalski-Płock-Podkowa Leśna-Polkowice-Poznań-Pruszków-Przemyśl-Puławy-Pułtusk-Racibórz-Radom-Radomsko-Ruda Śląska-Rybnik-Ryki-Rzeszów-Sandomierz-Sanok-Siedlce-Siemianowice Śląskie-Siemiatycze-Sieradz-Skarżysko-Kamienna-Skierniewice-Słupsk-Sochaczew-Sopot-Sosnowiec-Stalowa Wola-Starachowice-Stargard Szczeciński-Starogard Gdański-Sucha Beskidzka-Sulechów-Suwałki-Szczecin-Szczytno-Środa Wielkopolska-Świdnica-Świecie-Świętochłowice-Tarnobrzeg-Tarnowskie Góry-Tarnów-Tczew-Tomaszów Mazowiecki-Toruń-Tuchola-Tychy-Ustroń-Wałbrzych-Wałcz-Warszawa-Wejherowo-Włocławek-Wodzisław Śląski-Wołomin-Wrocław-Wyszków-Zabrze-Zamość-Zawiercie-Zgierz-Zielona Góra-Żary-Żory-Żyrardów-Żywiec";
        List<String> miasta = Arrays.asList(txt.split("-"));
        for (String m : miasta) {
            System.out.println(m);
        }
    }

    @RequestMapping("/map")
    public String showMap() {
        return "offer/map";
    }
	
	@RequestMapping("/contact")
	public String contact() {
		return "contact";
	}

    public void loadApartments(ModelMap model,HttpServletRequest request) {

        if(apartmentService.getAllApartments().isEmpty())	{
            model.put("msgtitle", "Katalog kwater");
            model.put("msg", "Lista ofert jest pusta.");
           // return "info";
        }

        System.out.println("wykonuje sie loadApartments()");

        PagedListHolder<ApartmentEntity> pagedListHolder = new PagedListHolder<ApartmentEntity>(apartmentService.getAllApartments());
        int page = ServletRequestUtils.getIntParameter(request, "p", 0);

        pagedListHolder.setPage(page);
        pagedListHolder.setPageSize(6);

        model.put("pagedListHolder", pagedListHolder);
        model.put("apartments", apartmentService.getAllApartments());
    }

}
