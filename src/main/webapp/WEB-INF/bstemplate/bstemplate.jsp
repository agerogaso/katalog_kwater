<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BANK KWATER - bstemplate</title>

    <link href=" <c:url value='/resources/css/jquery-ui.min.css'/> " rel="stylesheet" media="screen">
    <link href=" <c:url value='/resources/css/bootstrap.min.css'/> " rel="stylesheet" media="screen">
    <link href=" <c:url value='/resources/css/mystyle.css'/> " rel="stylesheet" media="screen">

</head>

<body>

    <tiles:insertAttribute name="header" />
    <tiles:insertAttribute name="content"/>
    <tiles:insertAttribute name="footer"/>

<script src="<c:url value='/resources/external/jquery/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/jquery-ui.min.js'/>"></script>
<%--<script src="<c:url value='/resources/js/jquery-2.1.1.min.js'/>"></script> --%>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function () {


        $("#navbar-links-ul").on("mouseover", "li", function() {
            $(this).stop(true, true).addClass("highlight-link", 100);
            $(this).find("a").css({ color: "black" } );


        });

        $("#navbar-links-ul").on("mouseout", "li", function() {

            $(this).stop(true,true).removeClass("highlight-link", 100);
            $(this).find("a").css({ color: "white" } );

        });



    })

</script>
</body>
</html>
