<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<head>
<title>Log in</title>
<style>
.error {
	color: red;
	font-weight: bold;
}
</style>
</head>
<body>

	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="content">

			<div class="content">

				<sec:authorize access="isAnonymous()">
					Wprowadź swój login oraz hasło: <br>
					<form name="f" action="<c:url value='j_spring_security_check'/>"
						method="POST">
						<table>
							<tr>
								<td>Login:</td>
								<td><input type='text' name='j_username' /></td>
							</tr>
							<tr>
								<td>Hasło:</td>
								<td><input type='password' name='j_password'></td>
							</tr>

							<tr>
								<td colspan='2'><input name="submit" type="submit"
									value="Log in!"></td>
							</tr>
						</table>
					</form>
					<br> Nie masz konta? <a href="register">Zarejestruj się.</a>!<br>


					<br>
					<a href="/bank_kwater"><< Wróć do strony głównej.</a>
				</sec:authorize>
				<sec:authorize access="isAuthenticated()">
					Jestes juz zalogowany.
				</sec:authorize>




			</div>

		</tiles:putAttribute>
	</tiles:insertDefinition>


</body>