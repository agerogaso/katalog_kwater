<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<head>
<title>Katalog kwater</title>
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" media="screen">
</head>
<body>
<%--
	<tiles:insertDefinition name="bootstrapTemplate">
		<tiles:putAttribute name="apartments">
--%>

			<div class="container">

				<c:if test="${not empty apartments}">
				
					<c:url value="/" var="pagedLink">
						<c:param name="action" value="list"/>
					    <c:param name="p" value="~"/>
					</c:url>
					<tg:paging pagedLink="${pagedLink }" pagedListHolder="${pagedListHolder }" />
					<br><br>
				
				
						<c:forEach var="a" items="${pagedListHolder.pageList }">

					    <div class="col-lg-4 col-md-6 col-sm-8 col-xs-12">
							<div class="apartment_thumb ">
								<div class="apartment_thumb_caption">
									<div class="caption" ><a href="/bank_kwater/apartments/${a.id }">${a.tytul }</a></div>
							
								</div>
								<div class="apartment_thumb_pic" >
									<img src="http://bd.vot.pl/bank_kwater/apartments/<c:out value="${a.id }"/>/zdjecie1.jpg" width="100%" height="180px" oncontextmenu="return false">
								</div>
								<div class="apartment_thumb_text">
								
									<div class="apartment_thumb_infoline">
										<p class="pleft">Ulica:</p>
										<p class="pright">${a.ulica }</p>
									</div>
									<div class="apartment_thumb_infoline">
										<p class="pleft">Ilość miejsc:</p>
										<p class="pright">${a.ilosc_miejsc }</p>
									</div>
									<div class="apartment_thumb_infoline">
										<p class="pleft">Cena za miejsce:</p>
										<p class="pright">${a.cena_miejsce } zł</p>
									</div>
									<div class="apartment_thumb_infoline">
										<p class="pleft">Cena za całość:</p>
										<p class="pright">${a.cena_calosc } zł</p>
									</div>
		
								</div>
							</div>
                        </div>
						</c:forEach>
				</c:if>

			</div>

<%--		</tiles:putAttribute>
	</tiles:insertDefinition>--%>



</body>