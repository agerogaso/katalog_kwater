<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<head>
<title>Błąd!</title>
</head>
<body>

	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="content">

			<div class="content">

				<c:if test="${not empty error}">
					<div style="color: red">Error :
						${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</div>
				</c:if>

				<p class="message"></p>
				<a href="/bank_kwater/login">Zaloguj się ponownie.</a>


			</div>

		</tiles:putAttribute>
	</tiles:insertDefinition>



</body>