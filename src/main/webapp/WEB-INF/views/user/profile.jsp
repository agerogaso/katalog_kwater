<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<head>
<title>Profil użytkownika</title>
</head>
<body>


	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="content">

			<div class="content">

				<sec:authentication property="principal.username" />

				<table>
					<tr>
						<td>Login:</td>
						<td>${user.userlogin }</td>
					</tr>

					<tr>
						<td>Imię:</td>
						<td>${user.imie }</td>
					</tr>
					<tr>
						<td>Nazwisko:</td>
						<td>${user.nazwisko }</td>
					</tr>
					<tr>
						<td>E-mail:</td>
						<td>${user.email }</td>
					</tr>
					<tr>
						<td>Telefon:</td>
						<td>${user.telefon }</td>
					</tr>

				</table>
				<a href="/bank_kwater/user/edit">Edytuj dane</a> <br><br>
				
				<a href="/bank_kwater/user/editpassword">Zmień hasło</a> <br>
			</div>

		</tiles:putAttribute>
	</tiles:insertDefinition>



</body>
