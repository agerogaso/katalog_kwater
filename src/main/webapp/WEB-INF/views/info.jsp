<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<title><c:if test="${not empty msgtitle }">${msgtitle }</c:if></title>
</head>
<body>
	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="content">

			<div class="content">
			
			<c:if test="${not empty msg }">
				${msg }
			</c:if>
						
				
			</div>

		</tiles:putAttribute>
	</tiles:insertDefinition>
</body>
