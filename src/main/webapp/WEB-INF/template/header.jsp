<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="/bank_kwater/">bank<span class="logo_colour">kwater</span></a></h1>
          <h2>Katalog kwater studenckich w Twoim mieście!</h2>
        </div>
      </div>
      <nav>
        <div id="menu_container">
          <ul class="sf-menu" id="nav">
            <li><a href="/bank_kwater">Home</a></li>
            <li><a href="/bank_kwater/apartments">Przeglądaj oferty</a></li>
            <li><a href="/bank_kwater/apartments/add">Dodaj ogłoszenie</a></li>
			<li><a href="/bank_kwater/contact">Kontakt</a></li>
            <li id="loginli"><a href="#">Login</a></li>

			<sec:authorize access="isAuthenticated()">
	            <li><a href="/bank_kwater/user/profile">Moje konto</a></li>
            </sec:authorize>
            
          </ul>
        </div>
      </nav>
    </header>

<script>
    $(document).ready(function() {
        $("#loginli" > a).click(function (event) {
            event.preventDefault();

            $(this).addClass("loginHighlighted");

        });
    })


</script>