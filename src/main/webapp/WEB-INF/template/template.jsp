<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>Bank kwater</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>" />
</head>

<body>
	<div id="main">
		<!-- HEADER -->
		<tiles:insertAttribute name="header"/>

		<div id="site_content">
			<!-- MENU --> 
			<tiles:insertAttribute name="menu"/>
			
			
			<!-- CONTENT -->
			<tiles:insertAttribute name="content"/>
		</div>


		<!-- FOOTER -->
		<tiles:insertAttribute name="footer"/>
	</div>


</body>
</html>
