<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-links" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value='/'/> "><img src="<c:url value='/resources/images/editicon.png'/> " class="logo"> </a>
        </div>

        <div id="navbar-links" class="navbar-collapse collapse">
            <ul id="navbar-links-ul" class="nav navbar-nav navbar-right">
                <li><a href="<c:url value='/'/>">HOME</a></li>
                <li><a href="<c:url value='/oferty'/>">OFERTY</a></li>
                <li><a href="<c:url value='/map'/>">MAPA</a></li>
                <li><a href="#">KONTO</a></li>
            </ul>
        </div>
    </div>
</div>

