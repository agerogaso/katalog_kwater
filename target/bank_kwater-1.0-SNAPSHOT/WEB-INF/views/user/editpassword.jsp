<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Edycja hasła</title>
</head>
<body>

	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="content">

			<div class="content">

				<form:form action="/bank_kwater/user/editpassword" commandName="userpasswordedit"
					method="POST">
					<table>
						<tr>
							<td>Stare hasło:</td>
							<td><form:password path="oldpass"  /></td>
							<td class="error"><form:errors path="oldpass" cssClass="error" />
							<p class="error">${errPass }</p></td>
						</tr>
						<tr>
							<td>Nowe hasło:</td>
							<td><form:password path="newuserpass" /></td>
							<td class="error"><form:errors path="newuserpass" cssClass="error" /></td>
						</tr>
						<tr>
							<td>Potwierdź nowe hasło:</td>
							<td><form:password path="newuserpass2" /></td>
							<td class="error"><form:errors path="newuserpass2" cssClass="error" /></td>
						</tr>
						<tr>
							<td><input type="submit" value="Edit" /></td>
						</tr>
					</table>
				</form:form>


			</div>

		</tiles:putAttribute>
	</tiles:insertDefinition>





</body>