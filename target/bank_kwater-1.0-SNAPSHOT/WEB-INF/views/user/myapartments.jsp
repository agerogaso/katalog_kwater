<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
	
<head>
<title>Moje oferty</title>
<script src="resources/js/jquery.confirm.js"></script>
</head>
<body>
	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="content">

			<div class="content">
			
				<c:url value="/user/myapartments" var="pagedLink">
					<c:param name="action" value="list"/>
				    <c:param name="p" value="~"/>
				</c:url>
				<tg:paging pagedLink="${pagedLink }" pagedListHolder="${pagedListHolder }" />
				<br><br>
				
				<c:forEach var="a" items="${pagedListHolder.pageList }">
				
					<div class="apartment_thumb">
						<div class="apartment_thumb_caption">
							<div class="caption" >${a.tytul }</div>
							
							<div class="deleteicon" ><a id="delete" href="/bank_kwater/apartments/<c:out value="${a.id }"/>/delete" ><img alt="Usuń" src="/bank_kwater/resources/images/deleteicon.png" width="20px" height="20px"></a></div>
							<div class="editicon"><a href="/bank_kwater/apartments/<c:out value="${a.id }"/>/edit"><img alt="Edytuj" src="/bank_kwater/resources/images/editicon.png" width="20px" height="20px"></a></div>
							
						</div>
						<div class="apartment_thumb_pic" >
							<img src="http://bd.vot.pl/bank_kwater/apartments/<c:out value="${a.id }"/>/zdjecie1.jpg" width="250px" height="180px" oncontextmenu="return false">
						</div>
						<div class="apartment_thumb_text">
						
							<div class="apartment_thumb_infoline">
								<p class="pleft">Ulica:</p>
								<p class="pright">${a.ulica }</p>
							</div>
							<div class="apartment_thumb_infoline">
								<p class="pleft">Ilość miejsc:</p>
								<p class="pright">${a.ilosc_miejsc }</p>
							</div>
							<div class="apartment_thumb_infoline">
								<p class="pleft">Cena za miejsce:</p>
								<p class="pright">${a.cena_miejsce } zł</p>
							</div>
							<div class="apartment_thumb_infoline">
								<p class="pleft">Cena za całość:</p>
								<p class="pright">${a.cena_calosc } zł</p>
							</div>

						</div>
					</div>		
				</c:forEach>
			</div>

		</tiles:putAttribute>
	</tiles:insertDefinition>



</body>