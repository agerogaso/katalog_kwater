<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Edycja profilu</title>
</head>
<body>

	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="content">

			<div class="content">

				<form:form action="/bank_kwater/user/edit" commandName="useredit"
					method="POST">
					<table>
						<tr>
							<td>Imie:</td>
							<td><form:input path="imie" value="${user.imie }" /></td>
							<td class="error"><form:errors path="imie" cssClass="error" /></td>
						</tr>
						<tr>
							<td>Nazwisko:</td>
							<td><form:input path="nazwisko"
									value="${user.nazwisko }" /></td>
							<td class="error"><form:errors path="nazwisko" cssClass="error" /></td>
						</tr>
						<tr>
							<td>E-mail:</td>
							<td><form:input path="email" value="${user.email }" /></td>
							<td class="error"><form:errors path="email" cssClass="error" /></td>
						</tr>
						<tr>
							<td>Telefon:</td>
							<td><form:input path="telefon"
									value="${user.telefon }" /></td>
							<td class="error"><form:errors path="telefon" cssClass="error" /></td>
						</tr>
						<tr>
							<td><input type="submit" value="Edit" /></td>
						</tr>
					</table>
				</form:form>


			</div>

		</tiles:putAttribute>
	</tiles:insertDefinition>





</body>