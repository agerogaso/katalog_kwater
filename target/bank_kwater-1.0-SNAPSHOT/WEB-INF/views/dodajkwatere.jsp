<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dodaj ogloszenie!</title>
<style>
.error {
    color: #ff0000;
    font-style: italic;
    font-weight: bold;
}
</style>
</head>
<body>

	<springForm:form action="/katalogkwater/kwatery/dodaj" method="POST"
		commandName="property">
		<table>
			<tr>
				<th>Tytul</th>
				<td><springForm:input path="tytul" /></td>
				<td><springForm:errors path="tytul" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Ulica</th>
				<td><springForm:input path="ulica" /></td>
				<td><springForm:errors path="ulica" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Dom</th>
				<td><springForm:input path="dom" /></td>
				<td><springForm:errors path="dom" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Miasto</th>
				<td><springForm:input path="miasto" /></td>
				<td><springForm:errors path="miasto" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Odleglosc od budynku glownego PWSZ w Nysie (km)</th>
				<td><springForm:input path="odleglosc" /></td>
				<td><springForm:errors path="odleglosc" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Stan ogolny</th>
				<td><springForm:input path="stan" /></td>
				<td><springForm:errors path="stan" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Powierzchnia kwatery (m2)</th>
				<td><springForm:input path="powierzchnia" /></td>
				<td><springForm:errors path="powierzchnia" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Rodzaj</th>
				<td><springForm:input path="rodzaj" /></td>
				<td><springForm:errors path="rodzaj" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Pietro</th>
				<td><springForm:input path="pietro" /></td>
				<td><springForm:errors path="pietro" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Ilosc pokoi</th>
				<td><springForm:input path="ilosc_pokoi" /></td>
				<td><springForm:errors path="ilosc_pokoi" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Ilosc miejsc</th>
				<td><springForm:input path="ilosc_miejsc" /></td>
				<td><springForm:errors path="ilosc_miejsc" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Cena od miejsca (zl)</th>
				<td><springForm:input path="cena_miejsce" /></td>
				<td><springForm:errors path="cena_miejsce" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Cena za calosc (zl)</th>
				<td><springForm:input path="cena_calosc" /></td>
				<td><springForm:errors path="cena_calosc" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Czy ceny za wynajem kwatery uwzgledniaja oplaty za media?</th>
				<td><springForm:input path="media_w_cenie" /></td>
				<td><springForm:errors path="media_w_cenie" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Czy wejscie do kwatery jest wspolne z innymi lokatorami
					mieszkania czy oddzielne?</th>
				<td><springForm:input path="wspolne_wejscie" /></td>
				<td><springForm:errors path="wspolne_wejscie" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Plec</th>
				<td><springForm:input path="plec" /></td>
				<td><springForm:errors path="plec" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Para/Malzenstwo</th>
				<td><springForm:input path="para" /></td>
				<td><springForm:errors path="para" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Palacy</th>
				<td><springForm:input path="palacy" /></td>
				<td><springForm:errors path="palacy" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Internet</th>
				<td><springForm:input path="internet" /></td>
				<td><springForm:errors path="internet" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Opis Internetu</th>
				<td><springForm:input path="opis_internet" /></td>
				<td><springForm:errors path="opis_internet" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Kuchnia</th>
				<td><springForm:input path="kuchnia" /></td>
				<td><springForm:errors path="kuchnia" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Opis kuchni</th>
				<td><springForm:input path="opis_kuchni" /></td>
				<td><springForm:errors path="opis_kuchni" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Lazienka</th>
				<td><springForm:input path="lazienka" /></td>
				<td><springForm:errors path="lazienka" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Opis lazienki</th>
				<td><springForm:input path="opis_lazienki" /></td>
				<td><springForm:errors path="opis_lazienki" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Goscie</th>
				<td><springForm:input path="goscie" /></td>
				<td><springForm:errors path="goscie" cssClass="error" /></td>
			</tr>
			<tr>
				<th>Dodatkowe Informacje</th>
				<td><springForm:input path="dodatkowe_info" /></td>
				<td><springForm:errors path="dodatkowe_info" cssClass="error" /></td>
			</tr>
			
			<tr>
			<td>
				<input type="submit" value="Dodaj!">
			</td>
			</tr>
		</table>

	</springForm:form>

</body>
</html>