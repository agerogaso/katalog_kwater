<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<title>Rejestracja uzytkownika</title>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
</head>
<body>

	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="content">

			<div class="content">

				<sec:authorize access="isAnonymous()">
					<springForm:form action="/bank_kwater/register" commandName="userregister"
					method="POST">
					<table>
						<tr>
							<td>Login:</td>
							<td><springForm:input path="userlogin" /></td>
							<td class="error"><springForm:errors path="userlogin" cssClass="error" />
								<p class="error">${errLogin }</p></td>
						</tr>

						<tr>
							<td>Haslo:</td>
							<td><springForm:password path="userpass" /></td>
							<td class="error"><springForm:errors path="userpass" cssClass="error" />
								<p class="error">${errPass }</p></td>
						</tr>

						<tr>
							<td>Powtorz haslo:</td>
							<td><springForm:password path="userpass2" /></td>
							<td class="error"><springForm:errors path="userpass2" cssClass="error" /></td>
						</tr>
						<tr>
							<td>Imie:</td>
							<td><springForm:input path="imie" /></td>
							<td class="error"><springForm:errors path="imie" cssClass="error" /></td>
						</tr>
						<tr>
							<td>Nazwisko:</td>
							<td><springForm:input path="nazwisko" /></td>
							<td class="error"><springForm:errors path="nazwisko" cssClass="error" /></td>
						</tr>
						<tr>
							<td>E-mail:</td>
							<td><springForm:input path="email" /></td>
							<td class="error"><springForm:errors path="email" cssClass="error" /></td>
						</tr>
						<tr>
							<td>Telefon:</td>
							<td><springForm:input path="telefon" /></td>
							<td class="error"><springForm:errors path="telefon" cssClass="error" /></td>
						</tr>
						<tr>
							<td><input type="submit" value="Register" /></td>
						</tr>
					</table>
				</springForm:form>

				</sec:authorize>
				<sec:authorize access="isAuthenticated()">
					Jesteś już zalogowany. Najpierw się <a href="<c:url value='j_spring_security_logout'/>">wyloguj</a> aby założyć nowe konto!
				</sec:authorize>

				


			</div>

		</tiles:putAttribute>
	</tiles:insertDefinition>

</body>