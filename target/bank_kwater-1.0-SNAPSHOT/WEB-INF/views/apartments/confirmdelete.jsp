<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<head>
<title><c:if test="${not empty msgtitle }">${msgtitle }</c:if></title>
</head>
<body>
	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="content">

			<div class="content">
			
			<c:if test="${not empty msg }">
				${msg }
				
				<form:form action="/bank_kwater/apartments/${apartment.id }/delete" method="POST">
					<input type="hidden" value="${apartment.id }"/>
					<input type="submit" value="Tak"/>
				</form:form>
				<br>
				<a href="/bank_kwater/user/myapartments">Anuluj...</a>
			</c:if>
						
				
			</div>

		</tiles:putAttribute>
	</tiles:insertDefinition>
</body>
