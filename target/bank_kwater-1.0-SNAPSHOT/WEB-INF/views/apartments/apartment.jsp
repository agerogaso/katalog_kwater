<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>
<title>Kwatera ${apartment.id }</title>
</head>
<body>

	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="content">

			<div class="content">


				<table>
					<tr>
						<th>Tytuł</th>
						<td>${apartment.tytul }</td>
					</tr>
					<tr>
						<th>Ulica</th>
						<td>${apartment.ulica }</td>
					</tr>
					<tr>
						<th>Numer domu:</th>
						<td>${apartment.dom }</td>
					</tr>
					<tr>
						<th>Miasto</th>
						<td>${apartment.miasto }</td>
					</tr>
					<tr>
						<th>Odległość od budynku głównego PWSZ w Nysie (km)</th>
						<td>${apartment.odleglosc }</td>
					</tr>
					<tr>
						<th>Stan ogólny</th>
						<td>${apartment.stan }</td>
					</tr>
					<tr>
						<th>Powierzchnia kwatery (m2)</th>
						<td>${apartment.powierzchnia }</td>
					</tr>
					<tr>
						<th>Rodzaj</th>
						<td>${apartment.rodzaj }</td>
					</tr>
					<tr>
						<th>Piętro</th>
						<td>${apartment.pietro }</td>
					</tr>
					<tr>
						<th>Ilość pokoi</th>
						<td>${apartment.ilosc_pokoi }</td>
					</tr>
					<tr>
						<th>Ilość miejsc</th>
						<td>${apartment.ilosc_miejsc }</td>
					</tr>
					<tr>
						<th>Cena od miejsca (zł)</th>
						<td>${apartment.cena_miejsce }</td>
					</tr>
					<tr>
						<th>Cena za całość (zł)</th>
						<td>${apartment.cena_calosc }</td>
					</tr>
					<tr>
						<th>Czy ceny za wynajem kwatery uwzględniają opłaty za media?</th>
						<td>${apartment.media_w_cenie }</td>
					</tr>
					<tr>
						<th>Czy wejście do kwatery jest wspólne z innymi lokatorami
							mieszkania czy oddzielne?</th>
						<td>${apartment.wspolne_wejscie }</td>
					</tr>
					<tr>
						<th>Płeć</th>
						<td>${apartment.plec }</td>
					</tr>
					<tr>
						<th>Para/Malżeństwo</th>
						<td>${apartment.para }</td>
					</tr>
					<tr>
						<th>Palący</th>
						<td>${apartment.palacy }</td>
					</tr>
					<tr>
						<th>Internet</th>
						<td>${apartment.internet }</td>
					</tr>
					<tr>
						<th>Opis Internetu</th>
						<td>${apartment.opis_internet }</td>
					</tr>
					<tr>
						<th>Kuchnia</th>
						<td>${apartment.kuchnia }</td>
					</tr>
					<tr>
						<th>Opis kuchni</th>
						<td>${apartment.opis_kuchni }</td>
					</tr>
					<tr>
						<th>Łazienka</th>
						<td>${apartment.lazienka }</td>
					</tr>
					<tr>
						<th>Opis łazienki</th>
						<td>${apartment.opis_lazienki }</td>
					</tr>
					<tr>
						<th>Goście</th>
						<td>${apartment.goscie }</td>
					</tr>
					<tr>
						<th>Dodatkowe Informacje</th>
						<td>${apartment.dodatkowe_info }</td>
					</tr>
					<tr>
						<th>Użytkownik</th>
						<td>${user.userlogin }</td>
					</tr>
					<tr>
						<th>Telefon</th>
						<td>${user.telefon }</td>
					</tr>
					<tr>
						<th>E-mail</th>
						<td>${user.email }</td>
					</tr>
					<tr>
						<th>Dodano</th>
						<td>${apartment.dodano }</td>
					</tr>
					<tr>
						<th>Zaktualizowano</th>
						<td>${apartment.zaktualizowano }</td>
					</tr>

				</table>

				<br> <a href="/bank_kwater/apartments"> << Wróć do listy
					ogłoszeń</a>


			</div>

		</tiles:putAttribute>
	</tiles:insertDefinition>


</body>
</html>