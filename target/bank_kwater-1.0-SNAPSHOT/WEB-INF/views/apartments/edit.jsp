<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<head>
<title>Kwatera ${apartment.id} - edycja</title>
</head>
<body>

	<tiles:insertDefinition name="defaultTemplate">
		<tiles:putAttribute name="content">

			<div class="content">

				<c:if test="${not empty error}">
					<div style="color: red">Error : Nie mozesz edytowac tego
						apartamentu!</div>
				</c:if>

				<c:if test="${empty error}">
					<form:form action="/bank_kwater/apartments/editapartment" method="POST"
						commandName="apartment" enctype="multipart/form-data">
						<table class="addapartment">
							<tr>
								<th>Tytuł</th>
								<td><form:input path="tytul" value="${apartment.tytul }" /></td>
								<td class="error"><form:errors path="tytul" cssClass="error" /></td>
								<td class="error">
									<form:input type="hidden" path="id" value="${apartment.id }"/>
									<form:errors path="id" cssClass="error" />
								</td>
							</tr>
							<tr>
								<th>Ulica</th>
								<td><form:input path="ulica"  value="${apartment.ulica }" /></td>
								<td class="error"><form:errors path="ulica"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Dom</th>
								<td><form:input path="dom"  value="${apartment.dom }" /></td>
								<td class="error"><form:errors path="dom" cssClass="error" /></td>
							</tr>
							<tr>
								<th>Miasto</th>
								<td><form:input path="miasto"  value="${apartment.miasto }" /></td>
								<td class="error"><form:errors path="miasto"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Odległość od budynku głównego PWSZ w Nysie (km)</th>
								<td><form:input path="odleglosc"  value="${apartment.odleglosc }" /></td>
								<td class="error"><form:errors path="odleglosc"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Stan ogólny</th>
								<td><form:input path="stan"  value="${apartment.stan }" /></td>
								<td class="error"><form:errors path="stan" cssClass="error" /></td>
							</tr>
							<tr>
								<th>Powierzchnia kwatery (m2)</th>
								<td><form:input path="powierzchnia" value="${apartment.powierzchnia }" /></td>
								<td class="error"><form:errors path="powierzchnia"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Rodzaj kwatery</th>
								<td><form:select path="rodzaj" >
										
									<form:option value="nieokreslono" label="Nieokreślono"></form:option>
									<form:option value="mieszkanie" label="Mieszkanie"></form:option>
									<form:option value="dom" label="Dom"></form:option>
					
										
										<%-- <c:if test="${ apartment.rodzaj == nieokreslono }"> <form:option value="nieokreslono" label="Nieokreślono"  selected="selected"></form:option> </c:if>
										<c:if test="${ apartment.rodzaj != nieokreslono }"> <form:option value="nieokreslono" label="Nieokreślono"  ></form:option> </c:if>
										
										<c:if test="${ apartment.rodzaj == mieszkanie }"> <form:option value="mieszkanie" label="Mieszkanie" selected="selected"></form:option> </c:if>
										<c:if test="${ apartment.rodzaj != mieszkanie }"> <form:option value="mieszkanie" label="Mieszkanie" ></form:option> </c:if>
										
										<c:if test="${ apartment.rodzaj == dom }"> <form:option value="dom" label="Dom" selected="selected"/> </c:if>
										<c:if test="${ apartment.rodzaj != dom }"> <form:option value="dom" label="Dom"  /> </c:if> --%>
										
									</form:select></td>
								<td class="error"><form:errors path="rodzaj"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Piętro</th>
								<td><form:input path="pietro" /></td>
								<td class="error"><form:errors path="pietro"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Ilość pokoi</th>
								<td><form:input path="ilosc_pokoi" /></td>
								<td class="error"><form:errors path="ilosc_pokoi"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Ilość miejsc</th>
								<td><form:input path="ilosc_miejsc" /></td>
								<td class="error"><form:errors path="ilosc_miejsc"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Cena od miejsca (zł)</th>
								<td><form:input path="cena_miejsce" /></td>
								<td class="error"><form:errors path="cena_miejsce"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Cena za całość (zł)</th>
								<td><form:input path="cena_calosc" /></td>
								<td class="error"><form:errors path="cena_calosc"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Czy ceny za wynajem kwatery uwzględniają opłaty za
									media?</th>
								<td><form:select path="media_w_cenie">
										<form:option value="nieokreslono" label="Nieokreślono"></form:option>
										<form:option value="tak" label="Tak"></form:option>
										<form:option value="nie" label="Nie"></form:option>
									</form:select></td>
								<td class="error"><form:errors path="media_w_cenie"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Czy wejście do kwatery jest wspólne z innymi lokatorami
									mieszkania?</th>
								<td><form:select path="wspolne_wejscie">
										<form:option value="nieokreslono" label="Nieokreślono"></form:option>
										<form:option value="tak" label="Tak"></form:option>
										<form:option value="nie" label="Nie"></form:option>
									</form:select></td>
								<td class="error"><form:errors path="wspolne_wejscie"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Płeć</th>
								<td><form:select path="plec">
										<form:option value="nieokreslono" label="Nieokreślono"></form:option>
										<form:option value="kobiety" label="Kobiety"></form:option>
										<form:option value="mezczyzni" label="Mezczyzni"></form:option>
										<form:option value="obojetne" label="Obojetnie"></form:option>
									</form:select></td>
								<td class="error"><form:errors path="plec" cssClass="error" /></td>
							</tr>
							<tr>
								<th>Para/Małżeństwo</th>
								<td><form:select path="para">
										<form:option value="nieokreslono" label="Nieokreślono"></form:option>
										<form:option value="tak" label="Tak"></form:option>
										<form:option value="nie" label="Nie"></form:option>
										<form:option value="obojetne" label="Obojetnie"></form:option>
									</form:select></td>
								<td class="error"><form:errors path="para" cssClass="error" /></td>
							</tr>
							<tr>
								<th>Palacy</th>
								<td><form:select path="palacy">
										<form:option value="nieokreslono" label="Nieokreślono"></form:option>
										<form:option value="tak" label="Tak"></form:option>
										<form:option value="nie" label="Nie"></form:option>
										<form:option value="obojetne" label="Obojetnie"></form:option>
									</form:select></td>
								<td class="error"><form:errors path="palacy"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Internet</th>
								<td><form:input path="internet" /></td>
								<td class="error"><form:errors path="internet"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Opis Internetu</th>
								<td><form:input path="opis_internet" /></td>
								<td class="error"><form:errors path="opis_internet"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Kuchnia</th>
								<td><form:input path="kuchnia" /></td>
								<td class="error"><form:errors path="kuchnia"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Opis kuchni</th>
								<td><form:input path="opis_kuchni" /></td>
								<td class="error"><form:errors path="opis_kuchni"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Łazienka</th>
								<td><form:input path="lazienka" /></td>
								<td class="error"><form:errors path="lazienka"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Opis łazienki</th>
								<td><form:input path="opis_lazienki" /></td>
								<td class="error"><form:errors path="opis_lazienki"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Goście</th>
								<td><form:input path="goscie" /></td>
								<td class="error"><form:errors path="goscie"
										cssClass="error" /></td>
							</tr>
							<tr>
								<th>Dodatkowe informacje</th>
								<td><form:textarea path="dodatkowe_info"
										style="height:100px; width: 100%;" /></td>
								<td class="error"><form:errors path="dodatkowe_info"
										cssClass="error" /></td>
							</tr>
		
							<tr>
								<td><input type="submit" value="Edytuj"></td>
							</tr>
						</table>

					</form:form>
				</c:if>


			</div>

		</tiles:putAttribute>
	</tiles:insertDefinition>
</body>
