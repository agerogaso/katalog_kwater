<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

    <title>Katalog Kwater - wybierz województwo</title>


    <tiles:insertDefinition name="bootstrapTemplate">
        <tiles:putAttribute name="content">
            <div class="container">

                <div class="col-md-7 col-sm-7 hidden-xs" >
                    <div id="map_area" >
                        <img id="map_poland" usemap="#poland" src=" <c:url value='/resources/images/polska_background.png'/> "/>

                        <ul id="map_ul">
                            <li id="dolnoslaskie"> <img src="<c:url value='/resources/images/dolnoslaskie.png'/>" usemap="#poland"> </li>
                            <li id="opolskie"> <img src="<c:url value='/resources/images/opolskie.png'/>" usemap="#poland"> </li>
                            <li id="kujawskopomorskie"> <img src="<c:url value='/resources/images/kujawskopomorskie.png'/>" usemap="#poland"> </li>
                            <li id="lodzkie"> <img src="<c:url value='/resources/images/lodzkie.png'/>" usemap="#poland"> </li>
                            <li id="lubelskie"> <img src="<c:url value='/resources/images/lubelskie.png'/>" usemap="#poland"> </li>
                            <li id="lubuskie"> <img src="<c:url value='/resources/images/lubuskie.png'/>" usemap="#poland"> </li>
                            <li id="malopolskie"> <img src="<c:url value='/resources/images/malopolskie.png'/>" usemap="#poland"> </li>
                            <li id="mazowieckie"> <img src="<c:url value='/resources/images/mazowieckie.png'/>" usemap="#poland"> </li>
                            <li id="podkarpackie"> <img src="<c:url value='/resources/images/podkarpackie.png'/>" usemap="#poland"> </li>
                            <li id="podlaskie"> <img src="<c:url value='/resources/images/podlaskie.png'/>" usemap="#poland"> </li>
                            <li id="pomorskie"> <img src="<c:url value='/resources/images/pomorskie.png'/>" usemap="#poland"> </li>
                            <li id="slaskie"> <img src="<c:url value='/resources/images/slaskie.png'/>" usemap="#poland"> </li>
                            <li id="swietokrzyskie"> <img src="<c:url value='/resources/images/swietokrzyskie.png'/>" usemap="#poland"> </li>
                            <li id="warminskomazurskie"> <img src="<c:url value='/resources/images/warminskomazurskie.png'/>" usemap="#poland"> </li>
                            <li id="wielkopolskie"> <img src="<c:url value='/resources/images/wielkopolskie.png'/>" usemap="#poland"> </li>
                            <li id="zachodniopomorskie"> <img src="<c:url value='/resources/images/zachodniopomorskie.png'/>" usemap="#poland"> </li>
                        </ul>

                        <map name="poland" id="poland">
                            <area shape="poly" alt="" title="" coords="51,572, 66,523, 63,491, 112,477, 123,488, 155,452, 184,461, 199,446, 217,454, 219,463, 250,482, 286,469, 304,481, 295,499, 312,505, 317,536, 295,545, 264,617, 251,636, 232,641, 247,674, 216,687, 185,651, 166,632, 196,620, 182,606, 153,613, 150,603, 139,602, 137,590, 98,584, 89,555, 69,549, 65,571, 54,569" href="#dolnoslaskie" target="" />
                            <area shape="poly" alt="" title="" coords="318,540, 329,547, 356,539, 401,554, 379,610, 394,626, 373,637, 371,670, 339,687, 332,706, 298,684, 316,676, 313,659, 273,667, 240,644, 265,616, 288,569, 303,540, 318,531" href="#opolskie" target="" />
                            <area shape="poly" alt="" title="" coords="406,554, 445,568, 458,556, 475,579, 494,583, 500,594, 491,606, 511,611, 510,643, 466,656, 456,683, 435,705, 465,740, 470,767, 449,773, 447,791, 429,792, 412,754, 386,721, 338,698, 375,662, 378,631, 393,624, 383,606, 399,553" href="#slaskie" target="" />
                            <area shape="poly" alt="" title="" coords="520,636, 542,640, 557,676, 625,654, 627,713, 653,785, 608,801, 586,785, 549,789, 529,823, 500,814, 503,792, 473,765, 469,738, 437,708, 464,678, 468,657, 511,636" href="#malopolskie" target="" />
                            <area shape="poly" alt="" title="" coords="689,591, 708,585, 714,604, 747,620, 736,636, 762,656, 793,655, 825,637, 839,655, 766,765, 775,809, 791,835, 713,808, 696,789, 652,780, 631,717, 630,649, 666,628, 690,592" href="#podkarpackie" target="" />
                            <area shape="poly" alt="" title="" coords="47,323, 58,315, 77,286, 101,293, 124,272, 143,278, 165,257, 164,291, 160,312, 144,319, 156,345, 150,399, 172,427, 199,446, 188,462, 164,442, 128,491, 112,477, 65,490, 50,476, 54,457, 40,429, 56,398, 40,356, 50,322" href="#lubuskie" target="" />
                            <area shape="poly" alt="" title="" coords="248,186, 257,203, 286,204, 280,222, 293,238, 285,273, 300,293, 295,316, 314,330, 333,329, 364,353, 391,350, 414,373, 426,370, 435,383, 420,396, 420,410, 403,412, 400,447, 383,442, 371,473, 371,501, 349,503, 347,524, 356,539, 325,544, 315,502, 300,497, 304,480, 286,463, 246,484, 222,463, 196,440, 177,428, 154,393, 160,345, 151,319, 166,310, 168,271, 200,272, 206,257, 235,233, 211,214, 230,204, 241,186" href="#wielkopolskie" target="" />
                            <area shape="poly" alt="" title="" coords="454,368, 489,387, 506,380, 539,407, 543,435, 569,437, 575,467, 548,461, 564,494, 549,519, 516,532, 521,563, 503,560, 496,586, 471,573, 453,552, 442,561, 397,547, 351,537, 347,509, 374,500, 384,443, 400,448, 407,412, 424,408, 429,395, 436,375" href="#lodzkie" target="" />
                            <area shape="poly" alt="" title="" coords="609,539, 647,557, 668,546, 681,560, 689,590, 681,608, 629,652, 569,678, 539,635, 508,627, 512,614, 493,601, 501,585, 505,560, 521,564, 521,535, 537,531, 555,511, 570,531" href="#swietokrzyskie" target="" />
                            <area shape="poly" alt="" title="" coords="794,374, 838,395, 838,438, 827,446, 834,476, 847,490, 842,510, 888,565, 878,576, 889,600, 886,630, 860,634, 843,655, 822,633, 809,638, 793,654, 759,650, 742,641, 752,631, 749,608, 711,604, 720,592, 709,580, 684,592, 681,477, 665,470, 686,454, 684,417, 717,410, 751,397, 770,402, 788,372" href="#lubelskie" target="" />
                            <area shape="poly" alt="" title="" coords="24,134, 40,130, 184,88, 234,47, 249,74, 244,99, 252,146, 248,184, 231,185, 233,204, 209,205, 214,219, 232,231, 207,248, 198,269, 171,267, 166,251, 152,270, 121,269, 108,288, 79,287, 54,320, 38,316, 6,284, 12,263, 34,255, 33,235, 40,229, 22,133" href="#zachodniopomorskie" target=""/>
                            <area shape="poly" alt="" title="" coords="354,167, 370,176, 407,182, 408,189, 440,195, 453,222, 483,232, 493,253, 481,265, 485,280, 466,288, 471,316, 454,367, 430,371, 406,366, 385,348, 354,347, 331,330, 314,329, 295,314, 298,280, 284,253, 292,234, 282,213, 297,189, 319,194, 332,175" href="#kujawskopomorskie" target="" />
                            <area shape="poly" alt="" title="" coords="542,259, 557,239, 600,223, 609,230, 650,202, 661,257, 691,283, 687,292, 712,292, 738,352, 776,361, 792,376, 764,400, 741,396, 680,410, 687,460, 665,474, 680,487, 681,555, 663,545, 645,554, 613,535, 568,528, 555,510, 566,493, 553,464, 578,468, 565,430, 542,429, 537,404, 508,377, 486,379, 455,366, 475,313, 465,284, 486,279, 491,257, 507,249, 518,251" href="#mazowieckie" target="" />
                            <area shape="poly" alt="" title="" coords="751,67, 810,111, 826,201, 849,245, 858,309, 842,321, 818,325, 794,368, 738,350, 710,287, 688,291, 660,253, 651,202, 673,208, 697,193, 745,157, 748,135, 734,112, 721,97, 747,78" href="#podlaskie" target="" />
                            <area shape="poly" alt="" title="" coords="482,70, 616,82, 740,74, 721,95, 745,139, 741,160, 676,207, 650,199, 609,230, 558,242, 538,258, 494,254, 485,231, 451,220, 438,190, 455,163, 471,163, 476,146, 446,117, 457,101, 452,92, 483,76" href="#warminskomazurskie" target="" />
                            <area shape="poly" alt="" title="" coords="311,24, 374,11, 397,77, 445,85, 479,70, 481,80, 449,93, 455,104, 443,117, 469,145, 469,165, 449,162, 437,192, 408,185, 403,173, 369,174, 352,160, 319,192, 297,190, 285,203, 253,191, 252,144, 240,100, 250,93, 242,48, 265,43, 282,27" href="#pomorskie" target="" />
                        </map>

                    </div>
                </div>
                <div class="col-md-5 col-sm-5 hidden-xs">
                    <p>
                        <h2>Wybierz województwo lub wprowadź nazwę miasta:</h2> <br>
                        <input id="city-search" type="text" />
                    </p>
                </div>
                <div class="col-xs-12 hidden-sm hidden-md hidden-lg" style="background-color: #67b168">
                    Wpisz nazwę miasta: :)
                </div>




            </div>
        </tiles:putAttribute>
    </tiles:insertDefinition>


<%--<script src="<c:url value='/resources/external/jquery/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/jquery-ui.min.js'/>"></script>--%>
    <script type="text/javascript">
        $(document).ready(function() {



            // resizeCoords();
            var resizeCoords = function(currWid) {
                $("area").each(function() {
                    var pairs = $(this).attr("data-coords").split(', ');
                    for(var i=0; i<pairs.length; i++) {
                        var nums = pairs[i].split(',');
                        for(var j=0; j<nums.length; j++) {
                            //nums[j] = parseFloat(nums[j]) * widthchange;
                            nums[j] = parseFloat(nums[j]) * (currWid/900);
                        }
                        pairs[i] = nums.join(',');
                    }
                    $(this).attr("coords", pairs.join(', '));
                });
            };
            // end of resizeCoords();

            var resizeWojewodztwa = function (w,h) {
                $("ul").find("img").each(function() {
                    var currArea = $(this);
                    currArea.width(w);
                    currArea.height(h);
                });
            };

            var resizeMap = function(w,h) {
                $("#map_poland").width(w);
                $("#map_poland").height(h);
                $("#map_area").width(w);
                $("#map_area").height(h);
                resizeCoords(w);
                resizeWojewodztwa(w,h);
            };


            // iteruje po kazdym elemencie <area> i nadaje mu nowy attrybut "data-coords" i wpisuje w nim wartosci aktualne z "coords"
            $("area").each(function() {
                $(this).attr('data-coords', $(this).attr('coords'))
            });


            if($(window).width() >= 1200) {
                console.log("START width >= 1200");
                resizeMap(600,568);
            }

            if( ($(window).width() >= 992) && ($(window).width() < 1200) ) {
                console.log("992 < START width < 1200");
                resizeMap(520,492);
            }

            if( $(window).width() >= 750 && ($(window).width() < 992) ) {
                console.log("750 < START width  < 992");
                resizeMap(400,379);
            }

            $( window ).resize(function() {

                if($(window).width() >= 1200) {
                    console.log(" width >= 1200");
                    resizeMap(600,568);
                }

                if( ($(window).width() >= 992) && ($(window).width() < 1200) ) {
                    console.log("992 <  width < 1200");
                    resizeMap(520,492);
                }

                if( $(window).width() >= 768 && ($(window).width() < 992) ) {
                    console.log("768 <  width  < 992");
                    resizeMap(400,379);
                }

            });


            $("#map_area").on("mouseover", "area", function () {
                var href = $(this).attr("href");
                $(href).stop(true,true).fadeIn("fast");
            });

            $("#map_area").on("mouseleave", "area", function () {
                var href = $(this).attr("href");
                $(href).stop(true,true).fadeOut("fast");
            });


            $("#city-search").autocomplete( {
                minLength: 1,

                source: function (request, response) {
                    $.getJSON("http://localhost:8080/bank_kwater/jsontest3", { term: request.term }, function(result) {
                        response($.map(result, function(item) {
                            return {
                                label: item.name,
                                value: item.name
                            }
                        }));
                    });
                }
            });





        });
    </script>
