<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="sidebar_container">
	<div class="sidebar">

		<sec:authorize access="isAnonymous()">
			Witaj gościu!<br>
			<a href="/bank_kwater/login">Zaloguj sie</a>
			<br><br>
			Nie masz konta? <br>
			<a href="/bank_kwater/register">Zarejestruj sie!</a>
		</sec:authorize>

		<sec:authorize access="isAuthenticated()">
			Witaj <sec:authentication property="principal.username" />!<br>
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				Jestes obecnie zalogowany jako administrator.<br>

				<a href="/bank_kwater/admin">Administrator page</a>
				<br>
				<a href="<c:url value='/bank_kwater/j_spring_security_logout'/>">Wyloguj sie</a>

			</sec:authorize>

			<sec:authorize access="hasRole('ROLE_USER')">

				<br>
				<a href="/bank_kwater/user/profile">Mój profil</a>
				<br>
				<a href="/bank_kwater/user/myapartments">Moje ogłoszenia</a>
				<br>
				<a href="<c:url value='/bank_kwater/j_spring_security_logout'/>">Logout</a>
			</sec:authorize>
		</sec:authorize>

	</div>

</div>






